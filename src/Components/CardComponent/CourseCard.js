import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
    card: {
        maxWidth: 300,
        backgroundColor: "#ffffff"
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

class CourseCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    toSection (courseId, courseName, courseDescription) {
        localStorage.setItem("courseId", courseId);
        localStorage.setItem("courseName", courseName);
        localStorage.setItem("courseDescription", courseDescription);
        this.props.prop.history.push('/section');
    }

    render() {
        const classes = this.props.classes;
        const bull = <span className={classes.bullet}>•</span>;
        return(
            <div>
                <Grid container className={classes.root} spacing={16} justify="flex-start" alignItems="center">

                {this.props.allCourse.map((each) => {
                    return (

                        <Grid item xs={4} onClick={()=>this.toSection(each.courseID, each.courseName, each.courseDescription)}>
                            <Card className={classes.card}>
                                <CardContent>

                                    <Typography variant="h5" component="h2">
                                        {each.courseID}
                                    </Typography>

                                    <Typography component="p">
                                        {each.courseName}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button size="small">See Sections</Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    )
                })
                }
                </Grid>

            </div>
        )
    }
}

CourseCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CourseCard);