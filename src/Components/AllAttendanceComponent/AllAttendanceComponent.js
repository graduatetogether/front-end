import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ListItem from '@material-ui/core/ListItem';
import axios from "../../AxiosConfiguration";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    white:{
        color: "#ffffff",
    },
    button: {
        margin: theme.spacing.unit,
        color: "#ffffff",
        borderColor: "#ffffff",
        float: "right",
    },
    button2: {
        margin: theme.spacing.unit,
        color: "#ffffff",
        borderColor: "#ffffff",
    },
    container: {
        display: "flex",
        justifyContent: "space-between",
    },
    dialog:{
        backgroundColor: "#ffffff",
    },
    green:{
        color: "#05b963"
    },
    description:{
        color: "#ffffff",
        marginRight: "15%",
    },
    table: {
        // minWidth: 700,
    },
    paper: {
        backgroundColor: "#ffffff"
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: "#e5e6e7",
        },
        // cursor: "pointer",
        onMouseEnter: {
            backgroundColor: "#000000"
        }
    },
    buttonQR: {
        marginTop: theme.spacing.unit,
        backgroundColor: "#05b963",
        color: "#ffffff",
        // borderColor: "#ffffff",
        float: "right",
    },
    hover :{
        backgroundColor: "#000000",
    }
});

let id = 0;
function createData(name, calories, fat, carbs, protein) {
    id += 1;
    return { id, name, calories, fat, carbs, protein };
}

const rows = [
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
    createData('5780001', "Lorem Ipsum", "10.00", "Present"),
];

const rows2 = [
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
    createData('5780001', "Lorem Ipsum", 3, 3, 0),
];

class AllAttendanceComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            openAddSection: false,
            toRender: "overview",
        }
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleClickOpenAddSection = () => {
        this.setState({ openAddSection: true });
    };

    handleCloseAddSection = () => {
        this.setState({ openAddSection: false });
    };

    toAttendance = () => {
        this.props.prop.history.push('/attendance')
        console.log("hi")
    };

    toSummary = () => {
        this.setState({toRender: "summary"})
    };

    toOverview = () => {
        this.setState({toRender: "overview"})
    };

    attemptToGenQr() {
        // event.preventDefault();
        console.log(`/start-generate-qr?courseID=${localStorage.getItem("courseId")}&sectionNumber=${localStorage.getItem("secNo")}&authkey=${localStorage.getItem("auth")}`);
        axios.post(`/start-generate-qr?courseID=${localStorage.getItem("courseId")}&sectionNumber=${localStorage.getItem("secNo")}&authkey=${localStorage.getItem("auth")}`)
            .then((response) => {
                console.log(response.data);
                axios.post(`/generate-qr-secret?courseID=${localStorage.getItem("courseId")}&sectionNumber=${localStorage.getItem("secNo")}&authkey=${localStorage.getItem("auth")}`)
                    .then((response) => {
                        console.log(response.data);
                        localStorage.setItem("secret", response.data.secret);
                        alert("Create QR complete");
                        window.open("qr", "_blank")
                    })
                    .catch((error) => {
                        console.log(error);
                        alert(`Cannot create QR due to ${error}`);
                    });
            })
            .catch((error) => {
                console.log(error);
                alert(`Cannot create QR due to ${error}`);
            });
    }

    render() {
        const classes = this.props.classes;
        return(
            <div>

                <Grid container className={classes.root} spacing={16} justify="space-between" alignItems="center">
                    <Grid item xs={6}>
                        <h2 className={classes.white}> {localStorage.getItem("courseId")} &nbsp; {localStorage.getItem("courseName")}</h2>
                    </Grid>
                    <Grid item xs={3}>
                        <Button variant="outlined" className={classes.button} onClick={()=>this.handleClickOpenAddSection()}>
                            Edit Section
                        </Button>
                    </Grid>
                </Grid>

                <Dialog
                    open={this.state.openAddSection}
                    onClose={()=>this.handleCloseAddSection()}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    disableBackdropClick
                >
                    <div className={classes.dialog}>

                        <DialogTitle id="alert-dialog-title">
                            <h2 className={classes.green}>
                                Edit Section
                            </h2>
                        </DialogTitle>

                        <DialogContent>
                            <TextField
                                id="Sec-no"
                                label="Sec No"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                            />

                            <TextField
                                id="time"
                                label="Time"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                            />

                            <TextField
                                id="date"
                                label="Date"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                            />

                            <TextField
                                id="Trimester"
                                label="Trimester"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                            />

                        </DialogContent>
                        <DialogActions>
                            <Button onClick={()=>this.handleCloseAddSection()} color="primary">
                                Cancel
                            </Button>
                            <Button onClick={()=>this.handleCloseAddSection()} color="primary" autoFocus>
                                Save
                            </Button>
                        </DialogActions>
                    </div>
                </Dialog>

                <Grid container className={classes.root} spacing={16} justify="flex-start" alignItems="center">
                    <Grid item xs={2}>
                        <h3 className={classes.description}>
                            Section {localStorage.getItem("secNo")}
                        </h3>
                    </Grid>
                    <Grid item xs={3}>
                        <h3 className={classes.description}>
                            {localStorage.getItem("dateTime")}
                        </h3>
                    </Grid>
                    <Grid item xs={5}>
                        <h2 className={classes.description}>
                            Enroll Code: {localStorage.getItem("EnrollCode")}
                        </h2>
                    </Grid>
                </Grid>

                {this.state.toRender === "overview" ? (
                    <div>
                        <Grid container className={classes.root} spacing={16} justify="flex-end" alignItems="center" >
                            <Grid item xs={3}>
                                <Button variant="outlined" className={classes.button} onClick={()=>this.toSummary()}>
                                    Attendance Summary
                                </Button>
                            </Grid>
                            <Grid item xs={3}>
                                <Button variant="outlined" className={classes.buttonQR} onClick={()=> this.attemptToGenQr()}>
                                    Create Attendance QR
                                </Button>
                            </Grid>
                        </Grid>


                        <br/>

                        <Paper className={classes.paper}>
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell numeric> Student ID </TableCell>
                                        <TableCell numeric> Name </TableCell>
                                        <TableCell numeric> Time </TableCell>
                                        <TableCell numeric> Status </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows.map(row => {
                                        return (
                                            <TableRow key={row.id} className={classes.row}>
                                                <TableCell component="th" scope="row">
                                                    {row.name}
                                                </TableCell>
                                                <TableCell numeric>{row.calories}</TableCell>
                                                <TableCell numeric>{row.fat}</TableCell>
                                                <TableCell numeric>{row.carbs}</TableCell>
                                                <TableCell numeric>{row.protein}</TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                            </Table>
                        </Paper>
                    </div>
                ) : (
                    <div>

                        <Grid container className={classes.root} spacing={16} justify="flex-end" alignItems="center">
                            <Grid item xs={3}>
                                <Button variant="outlined" className={classes.button} onClick={()=>this.toOverview()}>
                                    Attendance Overview
                                </Button>
                            </Grid>
                            <Grid item xs={3}>
                                <Button variant="outlined" className={classes.buttonQR}>
                                    Create Attendance QR
                                </Button>
                            </Grid>
                        </Grid>

                        <br/>

                        <Paper className={classes.paper}>
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell numeric> Student ID </TableCell>
                                        <TableCell numeric> Name </TableCell>
                                        <TableCell numeric> Present </TableCell>
                                        <TableCell numeric> Late </TableCell>
                                        <TableCell numeric> Absent </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows2.map(row => {
                                        return (
                                            <TableRow key={row.id} className={classes.row}>
                                                <TableCell component="th" scope="row">
                                                    {row.name}
                                                </TableCell>
                                                <TableCell numeric>{row.calories}</TableCell>
                                                <TableCell numeric>{row.fat}</TableCell>
                                                <TableCell numeric>{row.carbs}</TableCell>
                                                <TableCell numeric>{row.protein}</TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                            </Table>
                        </Paper>
                    </div>
                )}

            </div>
        )
    }
}



export default withStyles(styles)(AllAttendanceComponent);