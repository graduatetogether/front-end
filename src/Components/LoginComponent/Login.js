import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import classNames from 'classnames';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import './Login.css';
import axios from '../../AxiosConfiguration'

const styles = theme => ({
    root: {
        flexGrow: 1,
        textAlign: "center"
    },
    margin: {
        margin: theme.spacing.unit,
    },
    textField: {
        flexBasis: 300,
        // width: "95%",
        float: "left",
        marginTop: "5%",
        marginBottom: "5%"
    },
    button: {
        marginTop: "5%",
        // margin: theme.spacing.unit,
        backgroundColor: "#00bd58",
        color: "#ffffff"
    },
    center:{
        margin: 0,
        position: "absolute",
        top: "50%",
        left: "50%",
        msTransform: "translate(-50%, -50%)",
        transform: "translate(-50%, -50%)",
        minWidth: "350px",
        // minHeight: "350px"
    }
});

class Login extends Component {

    constructor(props) {
        super(props);
        this.renderSignup = this.renderSignup.bind(this);
        this.state = {
            email : "",
            password : "",
        }
    }

    toMain () {
        this.props.prop.history.push('/main');
    }

    renderSignup () {
        this.props.sendFunction();
    }

    handleChange = name => event => {
        this.setState({[name]: event.target.value});
    };

    attemptLogin() {
        // event.preventDefault();
        const { email, password} = this.state;
        console.log(email, password);
        axios.post(`/teacher-login?teacheremail=${email}&password=${password}`)
            .then((response) => {
                console.log(response.data);
                localStorage.setItem("auth", response.data.authKey);
                localStorage.setItem("login", "true");
                this.props.prop.history.push('/main');
            })
            .catch((error) => {
                console.log(error);
                alert(`Cannot Login due to ${error}` );
            })
    }

    render() {
        const classes = this.props.classes;

        return (
            <div className={classNames(classes.root, classes.center)}
                 style={{backgroundColor: "#ffffff", padding:"30px", borderRadius: "10px"}}>

                Maji

                <br/>

                <TextField
                    id="email-input"
                    label="Email"
                    className={classNames(classes.textField)}
                    name="email"
                    variant="outlined"
                    value={this.state.email}
                    onChange={this.handleChange('email')}
                    fullWidth={true}
                />

                <br/>

                <TextField
                    id="password-input"
                    label="Password"
                    className={classNames(classes.textField)}
                    type="password"
                    variant="outlined"
                    value={this.state.password}
                    onChange={this.handleChange('password')}
                    fullWidth={true}
                />

                <br/>

                <Button variant="contained" fullWidth={true} className={classes.button}
                        onClick={() => this.attemptLogin()}
                >
                    Login
                </Button>

                <br/>
                <br/>
                <br/>

                <h4 style={{fontWeight: "normal", display: "inline"}}>
                    Don't have an account? &nbsp;
                    <h4 style={{fontWeight: "normal", display: "inline", color: "#00bd58", cursor: "pointer",}}
                        onClick={()=>this.renderSignup()}
                    >
                        Sign up
                    </h4>
                </h4>






            </div>

        );
    }
}

export default withStyles(styles)(Login);