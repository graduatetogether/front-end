import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import classNames from 'classnames';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import './Login.css';
import axios from '../../AxiosConfiguration'

const styles = theme => ({
    root: {
        flexGrow: 1,
        textAlign: "center"
    },
    margin: {
        margin: theme.spacing.unit,
    },
    textField: {
        width: "100%",
        flexBasis: 300,
        float: "left",
        marginTop: "5%",
        // marginBottom: "5%"
    },
    button: {
        marginTop: "5%",
        // margin: theme.spacing.unit,
        backgroundColor: "#00bd58",
        color: "#ffffff"
    },
    center:{
        margin: 0,
        position: "absolute",
        top: "50%",
        left: "50%",
        msTransform: "translate(-50%, -50%)",
        transform: "translate(-50%, -50%)",
    }
});

class SignupComponent extends Component {

    constructor(props) {
        super(props);
        this.renderLogin = this.renderLogin.bind(this);
        this.state = {
            fname: "",
            lname: "",
            email : "",
            password : "",
            confirmPassword : "",
            disable: true,
        }
    }

    toMain () {
        this.props.prop.history.push('/main');
    }

    renderLogin () {
        this.props.sendFunction();
    }

    handleChange = name => event => {
        this.setState({[name]: event.target.value}, () => this.checkButtonDisable());
    };

    attemptSignUp() {
        // event.preventDefault();
        const { email, password, confirmPassword, fname, lname} = this.state;
        console.log(email, password, confirmPassword, fname, lname)
        if(password === confirmPassword){
            axios.post(`/teacher-register?teacheremail=${email}&teacherfname=${fname}&teacherlname=${lname}&password=${password}`)
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error)
                    alert(`Cannot Sign up due to ${error}` );
                });
            alert("Sign up complete");
            this.renderLogin()
        }else{
            this.setState({password : "", confirmPassword : ""});
            alert("The password and confirm password doesnt match");
        }
    }

    checkButtonDisable = () => {
        if (this.state.fname !== "" && this.state.lname !== "" && this.state.email !== "" && this.state.password !== "" && this.state.confirmPassword !== "") {
            this.setState({disable: false})
        }
        else{
            this.setState({disable: true})
        }
    };


    render() {
        const classes = this.props.classes;

        return (
            <div className={classNames(classes.root, classes.center)}
                 style={{backgroundColor: "#ffffff", padding:"30px", borderRadius: "10px"}}>

                SignUp

                <br/>

                <TextField
                    id="fname-input"
                    label="First Name"
                    className={classNames(classes.textField)}
                    name="fname"
                    variant="outlined"
                    value={this.state.fname}
                    onChange={this.handleChange('fname')}
                />


                <TextField
                    id="lname-input"
                    label="Last name"
                    className={classNames(classes.textField)}
                    name="lname"
                    variant="outlined"
                    value={this.state.lname}
                    onChange={this.handleChange('lname')}
                />

                <TextField
                    id="email-input"
                    label="Email"
                    className={classNames(classes.textField)}
                    name="email"
                    variant="outlined"
                    value={this.state.email}
                    onChange={this.handleChange('email')}
                />

                <TextField
                    id="password-input"
                    label="Password"
                    className={classNames(classes.textField)}
                    type="password"
                    variant="outlined"
                    value={this.state.password}
                    onChange={this.handleChange('password')}
                />

                <TextField
                    id="confirm-password-input"
                    label="Confirm Password"
                    className={classNames(classes.textField)}
                    type="password"
                    variant="outlined"
                    value={this.state.confirmPassword}
                    onChange={this.handleChange('confirmPassword')}
                />

                <Button variant="contained" fullWidth={true} className={classes.button}
                        onClick={() => this.attemptSignUp()}
                        disabled={this.state.disable}
                >
                    Sign up
                </Button>

                <br/>
                <br/>
                <br/>

                <h4 style={{fontWeight: "normal", display: "inline"}}>
                    Already have an account? &nbsp;
                    <h4 style={{fontWeight: "normal", display: "inline", color: "#00bd58", cursor: "pointer",}}
                        onClick={()=>this.renderLogin()}
                    >
                        Login
                    </h4>
                </h4>

            </div>

        );
    }
}

export default withStyles(styles)(SignupComponent);