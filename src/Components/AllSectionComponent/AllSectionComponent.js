import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ListItem from '@material-ui/core/ListItem';
import axios from "../../AxiosConfiguration";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    white:{
        color: "#ffffff",
    },
    button: {
        margin: theme.spacing.unit,
        color: "#ffffff",
        borderColor: "#ffffff",
        float: "right",
    },
    container: {
        display: "flex",
        justifyContent: "space-between",
    },
    dialog:{
        backgroundColor: "#ffffff",
    },
    green:{
        color: "#05b963"
    },
    description:{
        color: "#ffffff",
        marginRight: "15%",
    },
    table: {
        minWidth: 700,
    },
    paper: {
        backgroundColor: "#ffffff"
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: "#e5e6e7",
        },
        cursor: "pointer",
        onMouseEnter: {
            backgroundColor: "#000000"
        }
    },
    buttonSection: {
        marginTop: theme.spacing.unit,
        backgroundColor: "#05b963",
        color: "#ffffff",
        // borderColor: "#ffffff",
        // float: "right",
    },
    hover :{
        backgroundColor: "#000000",
}
});

let id = 0;
function createData(name, calories, fat, carbs, protein) {
    id += 1;
    return { id, name, calories, fat, carbs, protein };
}

const rows = [
    createData('Sec 1', "10.00 : 11.50", "Mon, Wed", "T1 - 2018", 30),
    createData('Sec 2', "12.00 : 13.50", "Mon, Wed", "T1 - 2018", 18),
    createData('Sec 3', "10.00 : 11.50", "Tue, Thu", "T1 - 2018", 23),
    createData('Sec 4', "12.00 : 13.50", "Tue, Thu", "T1 - 2018", 28),
];

class AllSectionComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            openAddSection: false,
            sectionNumber: 0,
            sectionRoom: "",
            date : "",
            time : "",
            disable: true,
        }
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleClickOpenAddSection = () => {
        this.setState({ openAddSection: true });
    };

    handleCloseAddSection = () => {
        this.setState({ openAddSection: false });
    };

    toAttendance = (SecNo, EnrollCode, dateTime) => {
        localStorage.setItem("secNo", SecNo);
        localStorage.setItem("EnrollCode", EnrollCode);
        localStorage.setItem("dateTime", dateTime);
        this.props.prop.history.push('/attendance');
    };

    checkButtonDisable = () => {
        if (this.state.sectionNumber !== "" && this.state.date !== "" && this.state.time !== "" && this.state.sectionRoom !== "") {
            this.setState({disable: false})
        }
        else{
            this.setState({disable: true})
        }
    };

    handleChange = name => event => {
        this.setState({[name]: event.target.value}, () => this.checkButtonDisable());
    };

    attemptCreateSectipn() {
        // event.preventDefault();
        const { sectionNumber, sectionRoom, date, time} = this.state;
        console.log(`/section-register?sectiondateandtime=${date + " " + time}&sectionnumber=${sectionNumber}&sectionroom=${sectionRoom}&courseid=${localStorage.getItem("courseId")}&authkey=${localStorage.getItem("auth")}`);
        axios.post(`/section-register?sectiondateandtime=${date + " " + time}&sectionnumber=${sectionNumber}&sectionroom=${sectionRoom}&courseid=${localStorage.getItem("courseId")}&authkey=${localStorage.getItem("auth")}`)
            .then((response) => {
                console.log(response.data);
                alert("Create section complete");
                this.setState({
                    openAddSection: false,
                    sectionNumber: 0,
                    sectionRoom: "",
                    date : "",
                    time : "",
                });
            })
            .catch((error) => {
                console.log(error);
                alert(`Cannot create section due to ${error}`);
            });
    }


    render() {
        const classes = this.props.classes;
        return(
            <div>

                <Grid container className={classes.root} spacing={16} justify="space-between" alignItems="center">
                    <Grid item xs={6}>
                        <h2 className={classes.white}> {localStorage.getItem("courseId")} &nbsp; {localStorage.getItem("courseName")} </h2>
                    </Grid>
                    <Grid item xs={3}>
                        <Button variant="outlined" className={classes.button} onClick={()=>this.handleClickOpen()}>
                            Edit Course
                        </Button>
                    </Grid>
                </Grid>

                <h3 className={classes.description}>
                    {localStorage.getItem("courseDescription")}
                </h3>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    disableBackdropClick
                >
                    <div className={classes.dialog}>

                        <DialogTitle id="alert-dialog-title">
                            <h2 className={classes.green}>
                                Edit Course
                            </h2>
                        </DialogTitle>

                        <DialogContent>
                            <TextField
                                id="course-id"
                                label="Course ID"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                            />

                            <TextField
                                id="course-name"
                                label="Course Name"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                            />

                            <TextField
                                id="Description"
                                label="Description"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                            />

                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                Cancel
                            </Button>
                            <Button onClick={this.handleClose} color="primary" autoFocus>
                                Save
                            </Button>
                        </DialogActions>
                    </div>
                </Dialog>

                <Paper className={classes.paper}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell> Section </TableCell>
                                <TableCell numeric> Date, Time </TableCell>
                                <TableCell numeric> Room </TableCell>
                                <TableCell numeric> Enroll code </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.allSections.map(row => {
                                return (
                                    <TableRow key={row.id} className={classes.row} onClick={()=>this.toAttendance(row.sectionUUID, row.enrollID, row.sectionDateAndTime)}>
                                        <TableCell numeric>{row.sectionUUID}</TableCell>
                                        <TableCell numeric>{row.sectionDateAndTime}</TableCell>
                                        <TableCell numeric>{row.room}</TableCell>
                                        <TableCell numeric>{row.enrollID}</TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </Paper>

                <Button variant="contained" className={classes.buttonSection}
                        onClick={() => this.handleClickOpenAddSection()}
                >
                    Add Section
                </Button>

                <Dialog
                    open={this.state.openAddSection}
                    onClose={()=>this.handleCloseAddSection()}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    disableBackdropClick
                >
                    <div className={classes.dialog}>

                        <DialogTitle id="alert-dialog-title">
                            <h2 className={classes.green}>
                                Add Section
                            </h2>
                        </DialogTitle>

                        <DialogContent>
                            <TextField
                                id="Sec-no"
                                label="Sec No"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                                value={this.state.sectionNumber}
                                onChange={this.handleChange('sectionNumber')}
                            />

                            <TextField
                                id="time"
                                label="Time (xx:00 - xx:50)"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                                value={this.state.time}
                                onChange={this.handleChange('time')}
                            />

                            <TextField
                                id="date"
                                label="Date (eg. Mon, Wed)"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                                value={this.state.date}
                                onChange={this.handleChange('date')}
                            />

                            <TextField
                                id="sectionRoom"
                                label="Section Room"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                                value={this.state.sectionRoom}
                                onChange={this.handleChange('sectionRoom')}
                            />

                        </DialogContent>
                        <DialogActions>
                            <Button onClick={()=>this.handleCloseAddSection()} color="primary">
                                Cancel
                            </Button>
                            <Button onClick={()=>this.attemptCreateSectipn()} color="primary" autoFocus disabled={this.state.disable}>
                                Save
                            </Button>
                        </DialogActions>
                    </div>
                </Dialog>

            </div>
        )
    }
}



export default withStyles(styles)(AllSectionComponent);