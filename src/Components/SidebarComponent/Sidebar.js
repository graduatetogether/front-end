import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import './Sidebar.css';
import Button from '@material-ui/core/Button';
// import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Hidden from '@material-ui/core/Hidden';
import MenuIcon from '@material-ui/icons/Menu';


const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        backgroundColor: "#3d4349",
        boxShadow: "unset",
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    drawerPaper: {
        width: drawerWidth,
        backgroundColor:"#3d4349",
    },
    drawerItem:{
        backgroundColor: "#454b51"
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
    },
    button: {
        marginTop: "auto",
        marginBottom: "10%",
        // margin: theme.spacing.unit,
        backgroundColor: "#e2014d",
        color: "#ffffff",
        width: "90%",
        alignSelf: "center",
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
});

class SideBar extends Component {
    constructor(props) {
        super(props);
        this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
        this.state = {
            // mobileOpen: false,
        }
    }

    handleDrawerToggle = () => {
        this.props.sendFunction();
    };

    logout () {
        localStorage.setItem("auth", "null");
        localStorage.setItem("login", "false");
        this.props.prop.history.push('/');
    }

    toSection () {
        this.props.prop.history.push('/section');
    }

    toCourse = () => {
        this.props.prop.history.push('/main');
    };

    toSetting = () => {
        this.props.prop.history.push('/setting');
    };

    toHelp = () => {
        this.props.prop.history.push('/help');
    };

    render() {
        const classes = this.props.classes;
        const prop = this.props;
        const theme = this.props;

        return (
            <div className={classes.root}>
                <CssBaseline />


                {prop.bar}

                {/*<AppBar position="fixed" className={classes.appBar}>*/}
                {/*<Toolbar>*/}

                {/*<IconButton*/}
                {/*aria-haspopup="true"*/}
                {/*color="inherit"*/}
                {/*>*/}
                {/*<AddIcon />*/}
                {/*</IconButton>*/}

                {/*</Toolbar>*/}
                {/*</AppBar>*/}


                <nav className={classes.drawer}>
                    {/* The implementation can be swap with js to avoid SEO duplication of links. */}
                    <Hidden smUp implementation="css">
                        <Drawer
                            container={this.props.container}
                            variant="temporary"
                            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                            open={prop.mobileOpen}
                            onClose={this.handleDrawerToggle}
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            ModalProps={{
                                keepMounted: true, // Better open performance on mobile.
                            }}
                        >
                            <div className={classes.toolbar} >
                                <h3 style={{textAlign: "center", color: "#ffffff"}}>
                                    aj Bolonga
                                </h3>
                            </div>
                            <Divider />

                            <List>

                                <ListItem button key="Course" className={classes.drawerItem} onClick={() => this.toCourse()}>
                                    <ListItemText>
                                        <h3 style={{color: "#ffffff"}}>
                                            Course
                                        </h3>
                                    </ListItemText>
                                </ListItem>
                                <Divider />

                                <ListItem button key="Setting" className={classes.drawerItem} onClick={() => this.toSetting()}>
                                    <ListItemText>
                                    <h3 style={{color: "#ffffff"}}>
                                        Setting
                                    </h3>
                                    </ListItemText>
                                </ListItem>
                                <Divider />

                                <ListItem button key="Help" className={classes.drawerItem} onClick={() => this.toHelp()}>
                                    <h3 style={{color: "#ffffff"}}>
                                        Help
                                    </h3>
                                </ListItem>
                                <Divider />

                            </List>
                            <Button variant="contained" fullWidth={true} className={classes.button}
                                    onClick={() => this.logout()}
                            >
                                Logout
                            </Button>
                        </Drawer>
                    </Hidden>
                    <Hidden xsDown implementation="css">
                        <Drawer
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            variant="permanent"
                            open
                        >
                            <div className={classes.toolbar} >
                                <h3 style={{textAlign: "center", color: "#ffffff"}}>
                                    aj Bolonga
                                </h3>
                            </div>
                            <Divider />

                            <List>

                                <ListItem button key="Course" className={classes.drawerItem} onClick={() => this.toCourse()}>
                                    <ListItemText>
                                        <h3 style={{color: "#ffffff"}}>
                                            Course
                                        </h3>
                                    </ListItemText>
                                </ListItem>
                                <Divider />

                                <ListItem button key="Setting" className={classes.drawerItem} onClick={() => this.toSetting()}>
                                    {/*<ListItemText>*/}
                                    <h3 style={{color: "#ffffff"}}>
                                        Setting
                                    </h3>
                                    {/*</ListItemText>*/}
                                </ListItem>
                                <Divider />

                                <ListItem button key="Help" className={classes.drawerItem} onClick={() => this.toHelp()}>
                                    <h3 style={{color: "#ffffff"}}>
                                        Help
                                    </h3>
                                </ListItem>
                                <Divider />

                            </List>
                            <Button variant="contained" fullWidth={true} className={classes.button}
                                    onClick={() => this.logout()}
                            >
                                Logout
                            </Button>
                        </Drawer>
                    </Hidden>
                </nav>
                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    {prop.data}
                </main>
            </div>
        );
    }
}



SideBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SideBar);