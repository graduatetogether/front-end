import React, { Component } from 'react';
import {withStyles} from "@material-ui/core/styles/index";
import PropTypes from 'prop-types';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AddIcon from '@material-ui/icons/AddCircleOutline';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        // flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
        backgroundColor: "#3d4349",
        boxShadow: "unset",
    },
    dialog:{
        backgroundColor: "#ffffff",
    },
    textField: {
        // marginLeft: theme.spacing.unit,
        // marginRight: theme.spacing.unit,
        // width: 400,
    },
    green:{
        color: "#05b963"
    }
});

class AppBarSection extends Component {
    constructor(props) {
        super(props);
        this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
        this.state = {
        }
    }

    handleDrawerToggle () {
        // do something
        this.props.sendFunction();
    }

    toCourse = () => {
        this.props.prop.history.push('/main');
    };

    render() {
        const classes = this.props.classes;
        return(
            <div>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                            onClick={this.handleDrawerToggle}
                        >
                            <MenuIcon />
                        </IconButton>
                        <h2 onClick={()=>this.toCourse()} style={{cursor: "pointer",}}>
                            Courses
                        </h2>
                        <h2>
                            &nbsp; > {localStorage.getItem("courseId")}
                        </h2>

                    </Toolbar>
                </AppBar>

            </div>

        )
    }
}



export default withStyles(styles)(AppBarSection);