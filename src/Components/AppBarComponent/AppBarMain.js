import React, { Component } from 'react';
import {withStyles} from "@material-ui/core/styles/index";
import PropTypes from 'prop-types';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AddIcon from '@material-ui/icons/AddCircleOutline';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import axios from "../../AxiosConfiguration";

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
        backgroundColor: "#3d4349",
        boxShadow: "unset",
    },
    dialog:{
        backgroundColor: "#ffffff",
    },
    textField: {
        // marginLeft: theme.spacing.unit,
        // marginRight: theme.spacing.unit,
        // width: 400,
    },
    green:{
        color: "#05b963"
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
});

class AppBarMain extends Component {
    constructor(props) {
        super(props);
        this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
        this.state = {
            open: false,
            courseId: "",
            courseName: "",
            description : "",
            year : "",
            trimester : "",
            disable: true,
        }
    }

    handleDrawerToggle () {
        this.props.sendFunction();
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    checkButtonDisable = () => {
        if (this.state.courseId !== "" && this.state.courseName !== "" && this.state.description !== "" && this.state.year !== "" && this.state.trimester !== "") {
            this.setState({disable: false})
        }
        else{
            this.setState({disable: true})
        }
    };

    handleChange = name => event => {
        this.setState({[name]: event.target.value}, () => this.checkButtonDisable());
    };

    attemptCreateCourse() {
        // event.preventDefault();
        const { courseId, courseName, description, year, trimester} = this.state;
        console.log(`/course-register?courseid=${courseId}&coursename=${courseName}&coursedescription=${description}&courseterm=T${trimester}_${year}&authkey=${localStorage.getItem("auth")}`)
        axios.post(`/course-register?courseid=${courseId}&coursename=${courseName}&coursedescription=${description}&courseterm=T${trimester}_${year}&authkey=${localStorage.getItem("auth")}`)
            .then((response) => {
                console.log(response.data);
                alert("Create complete");
                this.setState({
                    open: false,
                    courseId: "",
                    courseName: "",
                    description : "",
                    year : "",
                    trimester : "",
                });
            })
            .catch((error) => {
                console.log(error)
                alert(`Cannot create course due to ${error}`);
            });
    }

    render() {
        const classes = this.props.classes;
        return(
            <div>

                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon />
                        </IconButton>
                        <h2 className={classes.grow}>
                            Courses
                        </h2>
                        <IconButton aria-haspopup="true" color="inherit" onClick={() => this.handleClickOpen()}>
                            <AddIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    disableBackdropClick
                >
                    <div className={classes.dialog}>

                        <DialogTitle id="alert-dialog-title">
                            <h2 className={classes.green}>
                                Add Course
                            </h2>
                        </DialogTitle>

                        <DialogContent>
                            <TextField
                                id="course-id"
                                label="Course ID"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                                value={this.state.courseId}
                                onChange={this.handleChange('courseId')}
                            />

                            <TextField
                                id="course-name"
                                label="Course Name"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                                value={this.state.courseName}
                                onChange={this.handleChange('courseName')}
                            />

                            <TextField
                                id="Description"
                                label="Description"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                                value={this.state.description}
                                onChange={this.handleChange('description')}
                            />

                            <TextField
                                id="year"
                                label="Year"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                                value={this.state.year}
                                onChange={this.handleChange('year')}
                            />

                            <TextField
                                id="trimester"
                                label="Trimester"
                                className={classes.textField}
                                margin="normal"
                                fullWidth={true}
                                value={this.state.trimester}
                                onChange={this.handleChange('trimester')}
                            />

                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                Cancel
                            </Button>
                            <Button onClick={()=>this.attemptCreateCourse()} color="primary" autoFocus disabled={this.state.disable}>
                                Create
                            </Button>
                        </DialogActions>
                    </div>
                </Dialog>

            </div>


        )
    }
}



export default withStyles(styles)(AppBarMain);