import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import {
    BrowserRouter as Router,
    Route,
    withRouter,
    Switch,
} from 'react-router-dom'
import MuiThemeProvider from "@material-ui/core/es/styles/MuiThemeProvider";
import { createMuiTheme } from '@material-ui/core/styles';

import LoginAndSignupPage from './Pages/LoginAndSignupPage'
import Main from './Pages/MainPage'
import SectionPage from './Pages/SectionPage'
import AttendancePage from './Pages/AttendancePage'
import SettingPage from './Pages/SettingPage'
import HelpPage from './Pages/HelpPage'
import PrivateRoute from './PrivateRoute';
import QrPage from './Pages/QrPage'

const theme = createMuiTheme({

    palette: {
        background: '#2a3339',
        white: '#ffffff',
        dark: '#1b75d3',
        light: '#54abf3',
        buttonBlue: '#2f9bf4',
        buttonGreen: '#00c852',
        primaryColor: '#2296f3',
        secondaryFont: '#ababab',
        inherit: '#00bd58'
    },
});


class MainApp extends Component{
    constructor(props) {
        super(props);
        this.state = {
            authenticated: true,
        };
    }

    componentWillMount() {
        // this.setState({auth: localStorage.getItem("auth")}, () => this.checkAuth());
        console.log(localStorage.getItem("auth"));
        console.log(localStorage.getItem("login"));
            if (localStorage.getItem("login") !== "true"){
                this.setState({authenticated: false});
            }
    }

    checkAuth = () => {
        this.se3tState({authenticated: true});
    };


    render () {
        // const { classes } = this.props;
        const { authenticated} = this.state;
        return (
            <div >
                <MuiThemeProvider theme={theme}>
                    <Router>
                        <Switch>
                            <Route exact path="/" component={LoginAndSignupPage}/>
                            {/*<Route exact path="/qr" component={QrPage} />*/}
                            {/*<Route exact path="/section" component={SectionPage} />*/}
                            {/*<Route exact path="/attendance" component={AttendancePage} />*/}
                            {/*<Route exact path="/setting" component={SettingPage} />*/}
                            {/*<Route exact path="/help" component={HelpPage} />*/}
                            <PrivateRoute
                                exact path="/main"
                                component={Main}
                                authenticated={authenticated}
                            />
                            <PrivateRoute
                                exact path="/section"
                                component={SectionPage}
                                authenticated={authenticated}
                            />
                            <PrivateRoute
                                exact path="/attendance"
                                component={AttendancePage}
                                authenticated={authenticated}
                            />
                            <PrivateRoute
                                exact path="/setting"
                                component={SettingPage}
                                authenticated={authenticated}
                            />
                            <PrivateRoute
                                exact path="/help"
                                component={HelpPage}
                                authenticated={authenticated}
                            />
                            <PrivateRoute
                                exact path="/qr"
                                component={QrPage}
                                authenticated={authenticated}
                            />
                        </Switch>
                    </Router>
                </MuiThemeProvider>
            </div>
        )
    }

}


ReactDOM.render(<MainApp />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
