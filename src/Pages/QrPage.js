import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import axios from "../AxiosConfiguration";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
    whiteBg:{
        minWidth: "100%",
        minHeight: "1000px",
        backgroundColor: "#ffffff",
    },
    qr: {
        minWidth: "650px",
        minHeight: "650px",
        marginTop: "5%",
        // position: "absolute",
        // top: "50%",
        // left: "50%",
        // msTransform: "translate(-50%, -50%)",
        // transform: "translate(-50%, -50%)",
    },
    button: {
        // marginBottom: "10%",
        // margin: theme.spacing.unit,
        backgroundColor: "#e2014d",
        color: "#ffffff",
        // width: "90%",
        alignSelf: "center",
        minHeight: "50px",
        width: "500px",
    },
});

class QrPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            secret: "",
        };
        this.tick = this.tick.bind(this)
    }

    componentWillMount() {
        this.setState({ secret: localStorage.getItem("secret") });
        this.interval = setInterval(this.tick, 10000);
    }

    tick = () => {
        this.setState({secondsElapsed: this.state.secondsElapsed + 1});
        this.fetch();
    };

    fetch = () => {
        axios.post(`/generate-qr-secret?courseID=${localStorage.getItem("courseId")}&sectionNumber=${localStorage.getItem("secNo")}&authkey=${localStorage.getItem("auth")}`)
            .then((response) => {
                console.log(response.data);
                localStorage.setItem("secret", response.data.secret);
                this.setState({ secret: localStorage.getItem("secret") });
            })
            .catch((error) => {
                console.log(error);
                alert(`Cannot create QR due to ${error}`);
            });
    };

    shouldComponentUpdate(nextProps, nextState) {
        // console.log("hi");
        // console.log(this.state.secret)
        // console.log(nextState.secret)
        console.log(this.state.secret !== nextState.secret);
        if (this.state.secret !== nextState.secret) {
            return true;
        }
        return false;
    }

    render() {
        const classes = this.props.classes;
        var React = require('react');
        var QRCode = require('qrcode.react');
        return(
            <div className={classes.whiteBg} >
                <Grid container spacing={16} direction="column" justify="flex-start" alignItems="center">
                    <Grid item xs={12}>
                        <QRCode value={this.state.secret} className={classes.qr}/>
                    </Grid>

                    <Grid item xs={12}>
                        <Button variant="contained" fullWidth={true} className={classes.button}
                        >
                            Terminate QR
                        </Button>
                    </Grid>

                </Grid>
            </div>
        )
    }
}



export default withStyles(styles)(QrPage);