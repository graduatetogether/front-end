import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Sidebar from '../Components/SidebarComponent/Sidebar.js';
import AllSectionComponent from '../Components/AllSectionComponent/AllSectionComponent';
import AppBarSection from '../Components/AppBarComponent/AppBarSection';
import axios from "../AxiosConfiguration";


const styles = theme => ({

});

class SectionPage extends Component {
    constructor(props) {
        super(props);
        this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
        this.toAttendance = this.toAttendance.bind(this);
        this.state = {
            mobileOpen: false,
            sections: [],
        }
    }

    componentWillMount() {
        axios.get(`/get-section-from-course?courseid=${localStorage.getItem("courseId")}&authkey=${localStorage.getItem("auth")}`)
            .then((response) => {
                console.log(response.data);
                this.setState({sections: response.data})
            })
            .catch((error) => {
                console.log(error)
            });
    }

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    toAttendance = () =>{
        this.props.history.push("/attendance")
    };

    render() {
        const classes = this.props.classes;
        return(
            <div>


                <Sidebar data={<AllSectionComponent prop={this.props} sendFunction2={this.toAttendance} allSections={this.state.sections}/>}
                         bar={<AppBarSection sendFunction = {this.handleDrawerToggle} prop={this.props}/>}
                         prop={this.props}
                         mobileOpen={this.state.mobileOpen}
                         sendFunction = {this.handleDrawerToggle}
                />

            </div>
        )
    }
}



export default withStyles(styles)(SectionPage);