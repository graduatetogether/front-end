import React, { Component } from 'react';
import Login from '../Components/LoginComponent/Login';
import { withStyles } from '@material-ui/core/styles';
import SignupCompoenet from '../Components/LoginComponent/SignupComponent';


class LoginAndSignupPage extends Component {

    constructor(props) {
        super(props);
        this.renderSignup = this.renderSignup.bind(this);
        this.renderLogin = this.renderLogin.bind(this);
        this.state = {
            toRender: "login"
        }
    }

    renderSignup = () => {
        this.setState({ toRender: "signup" });
    };

    renderLogin = () => {
        this.setState({ toRender: "login" });
    };


    render() {
        return(
            <div>
                {this.state.toRender === "login" ? (
                    <Login
                        prop={this.props}
                        sendFunction = {this.renderSignup}
                    />
                ) : (
                    <SignupCompoenet
                        prop={this.props}
                        sendFunction = {this.renderLogin}
                    />
                )}

            </div>
        )
    }
}

export default (LoginAndSignupPage);