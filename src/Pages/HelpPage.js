import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Sidebar from '../Components/SidebarComponent/Sidebar.js';
import CourseCard from '../Components/CardComponent/CourseCard';
import NormalAppBar from '../Components/AppBarComponent/NormalAppBar';

const styles = theme => ({

});

class HelpPage extends Component {
    constructor(props) {
        super(props);
        this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
        this.state = {
            mobileOpen: false,
        }
    }

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    render() {
        const classes = this.props.classes;
        return(
            <div>

                <Sidebar data={<div/>}
                         bar={<NormalAppBar sendFunction = {this.handleDrawerToggle} title={"Help"}/>}
                         prop={this.props}
                         mobileOpen={this.state.mobileOpen}
                         sendFunction = {this.handleDrawerToggle}
                         toRender={"helpPage"}
                />

            </div>
        )
    }
}



export default withStyles(styles)(HelpPage);