import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Sidebar from '../Components/SidebarComponent/Sidebar.js';
import AllAttendanceComponent from '../Components/AllAttendanceComponent/AllAttendanceComponent';
import AppBarAttendance from '../Components/AppBarComponent/AppBarAttendance';

const styles = theme => ({

});

class AttendancePage extends Component {
    constructor(props) {
        super(props);
        this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
        this.state = {
            mobileOpen: false,
        }
    }

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    render() {
        const classes = this.props.classes;
        return(
            <div>
                <Sidebar data={<AllAttendanceComponent/>}
                         bar={<AppBarAttendance sendFunction = {this.handleDrawerToggle} prop={this.props}/>}
                         prop={this.props}
                         mobileOpen={this.state.mobileOpen}
                         sendFunction = {this.handleDrawerToggle}
                         toRender={"sectionPage"}
                />
            </div>
        )
    }
}



export default withStyles(styles)(AttendancePage);