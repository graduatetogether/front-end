import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Sidebar from '../Components/SidebarComponent/Sidebar.js';
import CourseCard from '../Components/CardComponent/CourseCard';
import AppBarMain from '../Components/AppBarComponent/AppBarMain';
import axios from "../AxiosConfiguration";

const styles = theme => ({

});

class MainPage extends Component {
    constructor(props) {
        super(props);
        this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
        this.state = {
            mobileOpen: false,
            courses: [],
        }
    }

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    componentWillMount() {
        axios.get(`/all-courses-by-teacher?authkey=${localStorage.getItem("auth")}`)
            .then((response) => {
                console.log(response.data);
                this.setState({courses: response.data})
            })
            .catch((error) => {
                console.log(error)
            });
    }


    render() {
        const classes = this.props.classes;
        return(
            <div>

                <Sidebar data={<CourseCard allCourse={this.state.courses} prop={this.props}/>}
                         bar={<AppBarMain sendFunction = {this.handleDrawerToggle}/>}
                         prop={this.props}
                         mobileOpen={this.state.mobileOpen}
                         sendFunction = {this.handleDrawerToggle}
                         toRender={"mainPage"}
                />

            </div>
        )
    }
}



export default withStyles(styles)(MainPage);